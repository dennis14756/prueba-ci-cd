require('dotenv').config()
const express = require('express')
const userRoutes = require('./users/routes')

const port = process.env.SERVER_PORT
const app = express()

app.use(express.json())

app.listen(port, () => {
  console.log(`server running on http://localhost:${port}`)
})

