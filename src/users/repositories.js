const { Sequelize } = require('sequelize')

let db
const databaseName = process.env.DB_NAME
const userName = process.env.DB_USERNAME
const password = process.env.DB_PASSWORD
const host = process.env.DB_HOST


const connect = async () => {
  if (!db) {
    try {
      console.log('Connecting to database...')
      db = new Sequelize(databaseName, userName, password, {
        dialect: 'mariadb',
        host
      })
      await db.authenticate()
      console.log('Connection has been established successfully')
    } catch (error) {
      console.error('Unable to connect to the database:', error)
    }
  }
}

connect()
